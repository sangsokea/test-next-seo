"use client";

import { useState } from "react";

const ENDPOINT = "http://localhost:3000/api/login";
const email = "sangsokea109@gmail.com";
const password = "admin@1234";

const productBody = {
	category: {
		name: "string",
		icon: "string",
	},
	name: "string",
	desc: "string",
	image: "string",
	price: "5.7",
	quantity: 2147483647,
};

export default function Login() {
	const [user, setUser] = useState<any>({});
	const [isError, setIsError] = useState(false);
	const handleLogin = async () => {
		const res = await fetch(ENDPOINT, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			body: JSON.stringify({ email: email, password: password }),
		});
		const data = await res.json();
		console.log(data);
		setUser(data);
	};

	const handleCreateProduct = async () => {
		const res = await fetch("http://localhost:8000/api/products/", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
				Authorization: `Bearer ${user?.accessToken || ""}`,
			},
			body: JSON.stringify(productBody),
		});
		if (res.status == 401) {
			console.log("Unauthorized");
			setIsError(true);
		}
		const data = await res.json();
		console.log(data);
	};

	const handleRefreshToken = async () => {
		const res = await fetch("http://localhost:3000/api/refresh", {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			credentials: "include",
		});
		const data = await res.json();
		console.log(data);
	};

	return (
		<div className="h-screen grid place-content-center ">
			<button onClick={handleLogin} className="text-4xl">
				Login
			</button>
			<button onClick={handleCreateProduct} className="text-4xl">
				Create Product
			</button>
			{isError && (
				<button onClick={handleRefreshToken} className="text-4xl">
					Refresh Token
				</button>
			)}
		</div>
	);
}
