import React from "react";
import styles from "./styles.module.css"

export default function page() {
	return (
		<>
			<div className={`${styles.container} text-6xl`}>Enroll</div>
		</>
	);
}
