import React from 'react'

type PropsParams = {
	params: { key: string };
	searchParams: { [key: string]: string | string[] | undefined };
};

    const ENDPOINT = "http://localhost:8000/account-confirm-email/";

    const getData = async (url:string,key: string): Promise<any> => {
        const res = await fetch(`${ENDPOINT}${url}/`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRFToken': 'zfiPJazO27n30J5Cx2n5soEgjutkX5sN63cjbhxFpOjV7LEXYT3qfm3zn307KPTl',
            },
            body: JSON.stringify({key: key})
            
        });console.log(res);
        const data = await res.json();
        console.log(data);
        return data;
    };

export default async function page({params, searchParams}: PropsParams) {
    const endcodedKey = params.key;
    const decodedKey = decodeURIComponent(endcodedKey);
     getData(endcodedKey,decodedKey);
    return (
    <div>decodedKey</div>
  )
}
