'use client'
import { Envelope } from "@/components/icons/FontAwesomeSvg";
import React from "react";
import style from "./style.module.css";
import Button from "./components/Button";

export default function ConfirmEmailVerification() {
	return (
		<main className={style.container}>
			<section className="flex flex-col items-center">
				{/* icon email */}
				<Envelope classname="w-36 h-36 mb-8" color="#080" />
				{/* main title */}
				<h1 className="text-5xl font-medium text-gray-700 my-4">
					Email has been confirmed!
				</h1>
				{/* description */}
				<p className="text-2xl my-4">
					After your email confirmed! Now you can login by click below button!
				</p>
                <p className="text-2xl my-4">
					សូមស្វាគមន៍ម្តងទៀត! អ្នកបានបញ្ជាក់អ៊ីមែលរបស់អ្នកហើយ!``
				</p>
				{/* button */}
                <Button classname="my-16" title="Login" onClick={() => console.log("login")} />
			</section>
		</main>
	);
}
