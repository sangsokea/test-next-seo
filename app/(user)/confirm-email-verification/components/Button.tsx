"use client";
import style from "./style.module.css";
type Props = {
	onClick: () => void;
	title: string;
    classname?: string;
};
export default function Button({ onClick, title, classname = "" }: Props) {
	return (
		<button onClick={onClick} className={`${style.container} ${classname}`}>
			{title}
		</button>
	);
}
