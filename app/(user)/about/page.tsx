import { Metadata } from 'next'
import React from 'react'


export const metadata: Metadata = {
  keywords: 'about, page',
}

export default function page() {
  return (
    <div className='h-screen grid place-content-center text-6xl'>About Page</div>
  )
}
