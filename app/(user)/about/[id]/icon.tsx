import { ImageResponse } from 'next/og'
import {PropsParams} from '@/lib/definitions'
// Route segment config
export const runtime = 'edge'
 
// Image metadata
export const size = {
  width: 32,
  height: 32,
}
export const contentType = 'image/png'
 
// Image generation
export default function Icon({params, searchParams}: PropsParams) {
  return new ImageResponse(
    (
      // ImageResponse JSX element
      <div
        style={{
          backgroundImage: 'url(https://source.unsplash.com/random)',
        }}
      >
        {params.id}
      </div>
    ),
    // ImageResponse options
    {
      // For convenience, we can re-use the exported icons size metadata
      // config to also set the ImageResponse's width and height.
      ...size,
    }
  )
}