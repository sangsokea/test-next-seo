export type PropsParams = {
	params: { [key:string]: string };
	searchParams: { [key: string]: string | string[] | undefined };
};